import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { PetanquePage } from '../pages/petanque/petanque';
import { AccountPage } from '../pages/account/account';
import { CalendrierPage } from '../pages/calendrier/calendrier';

import { AuthService } from '../services/authService.service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { IonicStorageModule } from '@ionic/storage';
import { Calendar } from '@ionic-native/calendar';


export const firebaseConfig = {
  apiKey: "AIzaSyD98PVQVbLVSIFUPEmZwb0OEDMD20LncfQ",
  authDomain: "suivisport-4aada.firebaseapp.com",
  databaseURL: "https://suivisport-4aada.firebaseio.com",
  projectId: "suivisport-4aada",
  storageBucket: "suivisport-4aada.appspot.com",
  messagingSenderId: "744465379112"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    PetanquePage,
    AccountPage,
    CalendrierPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['sqlite', 'websql', 'indexeddb']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    PetanquePage,
    AccountPage,
    CalendrierPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    Calendar,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
