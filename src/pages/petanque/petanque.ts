import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup } from '@angular/forms';

import { AngularFireDatabase  } from 'angularfire2/database';

import { Observable } from 'rxjs/Observable';
//import { AuthService } from '../../services/authService.service';

@Component({
  selector: 'page-petanque',
  templateUrl: 'petanque.html'
})

export class PetanquePage implements OnInit{

  public myConcours:Observable<any[]>;
  public displayFormConcours: boolean = false;
  public concoursForm: FormGroup;
  public nbConcours: number;
  public annee:number;
  public uid:string;

  private urlConcours: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private _storage: Storage,
    private db: AngularFireDatabase) {

  }

  public ngOnInit(): void {

    this.nbConcours = 0;
    this.annee = new Date().getFullYear();
    let that = this;
    this._storage.get('user').then((val) => {
      that.uid = val.uid;
      that.urlConcours = '/' + that.uid + '/concours';
      that.myConcours = that.db.list(that.urlConcours).valueChanges();
      that.getNbConcours(that.annee);
    });

    // init form
    this.initForm();
  }

  private initForm(): void {
    this.concoursForm = this.formBuilder.group({
      id: [''],
      ville: [''],
      club: [''],
      date: [''],
      annee: [''],
      formation: [''],
      nbEquipes: [''],
      nbPartiesGagnees: [''],
      nbPartiesPerdues: [''],
      resultat: [''],
      repechage: [''],
      points: ['']
    });
  }
  public addConcours(): void {
    this.displayFormConcours = true;
  }

  public cancel(): void {
    this.initForm();
    this.displayFormConcours = false;
  }

  private getNbConcours(year: number): void {
    this.nbConcours = 0;
    this.myConcours.forEach(element => {
      element.forEach(concours => {
        if(concours.annee == year){
          this.nbConcours++;
        }
      });
    });
  }

  public onChangeYear($event): void {
    this.getNbConcours($event);
  }

  public saveConcours(): void {
    const id:string = this.concoursForm.controls['id'].value
    let concoursToSave = {
      name: this.concoursForm.controls['club'].value ? this.concoursForm.controls['club'].value : '',
      ville: this.concoursForm.controls['ville'].value ? this.concoursForm.controls['ville'].value : '',
      date: this.concoursForm.controls['date'].value ? this.concoursForm.controls['date'].value : '',
      annee: new Date(this.concoursForm.controls['date'].value).getFullYear(),
      formation: this.concoursForm.controls['formation'].value ? this.concoursForm.controls['formation'].value : '',
      nbEquipes: this.concoursForm.controls['nbEquipes'].value ? this.concoursForm.controls['nbEquipes'].value : '',
      nbPartiesGagnees: this.concoursForm.controls['nbPartiesGagnees'].value ? this.concoursForm.controls['nbPartiesGagnees'].value : '',
      nbPartiesPerdues: this.concoursForm.controls['nbPartiesPerdues'].value ? this.concoursForm.controls['nbPartiesPerdues'].value : '',
      resultat: this.concoursForm.controls['resultat'].value ? this.concoursForm.controls['resultat'].value : '',
      repechage: this.concoursForm.controls['repechage'].value ? this.concoursForm.controls['repechage'].value : '',
      points: this.concoursForm.controls['points'].value ? this.concoursForm.controls['points'].value : ''
    };

    if(id && id !== null && id !== ''){
      this.db.list(this.urlConcours).update(id, concoursToSave);
    } else {
      this.db.list(this.urlConcours).push(concoursToSave);
    }

    this.displayFormConcours = false;
  }

  public editConcours(concours: any): void {
    this.concoursForm.controls['id'].setValue(concours.$key);
    this.concoursForm.controls['ville'].setValue(concours.ville);
    this.concoursForm.controls['club'].setValue(concours.name);
    this.concoursForm.controls['date'].setValue(concours.date);
    this.concoursForm.controls['formation'].setValue(concours.formation);
    this.concoursForm.controls['nbEquipes'].setValue(concours.nbEquipes);
    this.concoursForm.controls['nbPartiesGagnees'].setValue(concours.nbPartiesGagnees);
    this.concoursForm.controls['nbPartiesPerdues'].setValue(concours.nbPartiesPerdues);
    this.concoursForm.controls['resultat'].setValue(concours.resultat);
    this.concoursForm.controls['repechage'].setValue(concours.repechage);
    this.concoursForm.controls['points'].setValue(concours.points);

    this.displayFormConcours = true;
  }


}
