import {
  Injectable
} from '@angular/core';

import { Storage } from '@ionic/storage';

@Injectable()
export class AuthService {

  public name: string;
  public user: any;
  public userIsConnected: boolean;

  constructor(private _storage: Storage) { }

  public setUser(user: any): void {
    this.user = user;
    this._storage = user;
  }

  public getUser(): any {
    return this.user;
  }

  public setName(name: string):void {
    this.name = name;
  }

  public setConnected(): void {
    this.userIsConnected = true;
  }

  public setDisconnected(): void {
    this.userIsConnected = false;
  }

  public getUserIsConnected(): boolean {
    return this.userIsConnected;
  }
}
